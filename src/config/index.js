export default {
  baseUrl: 'static/images/map/',
  areas: [
    {
      id: 'a0',
      name: 'アバラシア雲海',
      img: 'a0.jpg',
      locations: [
        {name: 'A', id: 'a0a', img: 'a0_a.jpg', xy: [6.6, 7.2]}, // アバラシア雲海 ( 6.6  , 7.2 ) Z:1.5
        {name: 'B', id: 'a0b', img: 'a0_b.jpg', xy: [20.6, 5.3]}, // アバラシア雲海 ( 20.6  , 5.3 ) Z:2.5
        {name: 'C', id: 'a0c', img: 'a0_c.jpg', xy: [22.8, 10.7]}, // アバラシア雲海 ( 22.8  , 10.7 ) Z:2.5
        {name: 'D', id: 'a0d', img: 'a0_d.jpg', xy: [17, 17]},
        {name: 'E', id: 'a0e', img: 'a0_e.jpg', xy: [31, 18]},
        {name: 'F', id: 'a0f', img: 'a0_f.jpg', xy: [10.4, 39.1]}, // アバラシア雲海 ( 10.4  , 39.1 ) Z:0.1
        {name: 'G', id: 'a0g', img: 'a0_g.jpg', xy: [14.4, 26.9]}, // アバラシア雲海 ( 14.4  , 26.9 ) Z:0.8
        {name: 'H', id: 'a0h', img: 'a0_h.jpg', xy: [18.8, 34.9]}, // アバラシア雲海 ( 18.8  , 34.9 ) Z:0.7
        {name: 'I', id: 'a0i', img: 'a0_i.jpg', xy: [21, 31]},
        {name: 'J', id: 'a0j', img: 'a0_j.jpg', xy: [29.1, 24.1]} // アバラシア雲海 ( 29.1  , 24.1 ) Z:3.4
      ]
    },
    {
      id: 'a1',
      name: 'クルザス西部高地',
      img: 'a1.jpg',
      locations: [
        {name: 'A', id: 'a1a', img: 'a1_a.jpg', xy: [7, 12]},
        {name: 'B', id: 'a1b', img: 'a1_b.jpg', xy: [21, 12]},
        {name: 'C', id: 'a1c', img: 'a1_c.jpg', xy: [12, 24]},
        {name: 'D', id: 'a1d', img: 'a1_d.jpg', xy: [20.2, 30.9]}, // クルザス西部高地 ( 20.2  , 30.9 ) Z:0.5
        {name: 'E', id: 'a1e', img: 'a1_e.jpg', xy: [34.8, 7.7]}, // クルザス西部高地 ( 34.8  , 7.7 ) Z:0.7
        {name: 'F', id: 'a1f', img: 'a1_f.jpg', xy: [29.1, 15.5]}, // クルザス西部高地 ( 29.1  , 15.5 ) Z:0.9
        {name: 'G', id: 'a1g', img: 'a1_g.jpg', xy: [36, 26]}
      ]
    },
    {
      id: 'a2',
      name: '高地ドラヴァニア',
      img: 'a2.jpg',
      locations: [
        {name: 'A', id: 'a2a', img: 'a2_a.jpg', xy: [10.6, 30.1]}, // 高地ドラヴァニア ( 10.6  , 30.1 ) Z:0.8
        {name: 'B', id: 'a2b', img: 'a2_b.jpg', xy: [23.5, 25.5]},
        {name: 'C', id: 'a2c', img: 'a2_c.jpg', xy: [29.0, 24.0]}, // 高地ドラヴァニア ( 29.0  , 24.0 ) Z:0.9
        {name: 'D', id: 'a2d', img: 'a2_d.jpg', xy: [12.0, 35.0]}, // 高地ドラヴァニア ( 12.0  , 35.0 ) Z:0.9
        {name: 'E', id: 'a2e', img: 'a2_e.jpg', xy: [13.4, 34.6]}, // 高地ドラヴァニア ( 13.4  , 34.6 ) Z:0.7
        {name: 'F', id: 'a2f', img: 'a2_f.jpg', xy: [14.9, 34.4]}, // 高地ドラヴァニア ( 14.9  , 34.4 ) Z:1.2
        {name: 'G', id: 'a2g', img: 'a2_g.jpg', xy: [18.8, 34.1]}, // 高地ドラヴァニア ( 18.8  , 34.1 ) Z:0.9
        {name: 'H', id: 'a2h', img: 'a2_h.jpg', xy: [24, 32]},
        {name: 'I', id: 'a2i', img: 'a2_i.jpg', xy: [27, 30]}
      ]
    },
    {
      id: 'a3',
      name: 'ドラヴァニア雲海',
      img: 'a3.jpg',
      locations: [
        {name: 'A', id: 'a3a', img: 'a3_a.jpg', xy: [10.3, 9.6]}, // ドラヴァニア雲海 ( 10.3  , 9.6 ) Z:3.3
        {name: 'B', id: 'a3b', img: 'a3_b.jpg', xy: [12.8, 13.2]},
        {name: 'C', id: 'a3c', img: 'a3_c.jpg', xy: [7.9, 17.6]},  // ドラヴァニア雲海 ( 7.9  , 17.6 ) Z:3.3
        {name: 'D', id: 'a3d', img: 'a3_d.jpg', xy: [10.6, 20.3]},
        {name: 'E', id: 'a3e', img: 'a3_e.jpg', xy: [23.2, 19.2]},
        {name: 'F', id: 'a3f', img: 'a3_f.jpg', xy: [32.1, 25.6]}, // ラヴァニア雲海 ( 32.1  , 25.6 ) Z:0.5
        {name: 'G', id: 'a3g', img: 'a3_g.jpg', xy: [17.3, 33.7]}, // ドラヴァニア雲海 ( 17.3  , 33.7 ) Z:1.1
        {name: 'H', id: 'a3h', img: 'a3_h.jpg', xy: [21.6, 37.8]}, // ドラヴァニア雲海 ( 21.6  , 37.8 ) Z:0.1
        {name: 'I', id: 'a3i', img: 'a3_i.jpg', xy: [28.0, 37.5]} // ドラヴァニア雲海 ( 28.0  , 37.5 ) Z:0.9
      ]
    }
  ],
  aliases: [
    {id: 'a0', aliase: 'アバラシア雲海'},
    {id: 'a0', aliase: 'アバラシア'},
    {id: 'a0', aliase: 'アバ'},
    {id: 'a1', aliase: 'クルザス西部高地'},
    {id: 'a1', aliase: 'クルザス西部'},
    {id: 'a1', aliase: '西部'},
    {id: 'a2', aliase: '高地ドラヴァニア'},
    {id: 'a2', aliase: '高地ドラ'},
    {id: 'a2', aliase: 'ドラ高地'},
    {id: 'a3', aliase: 'ドラヴァニア雲海'},
    {id: 'a3', aliase: 'ドラ雲海'},
    {id: 'a3', aliase: 'ドラ雲'}
  ],
  aetherytes: [
    {idx: 0, name: 'キャンプ・クラウドトップ', areaId: 'a0', xy: [10.2, 33.6], weight: 7},
    {idx: 1, name: 'オク・ズンド', areaId: 'a0', xy: [10.4, 14.5], weight: 7},
    {idx: 2, name: 'ファルコンネスト', areaId: 'a1', xy: [31.9, 36.8], weight: 3},
    {idx: 3, name: 'テイルフェザーからクルザス西部へ', areaId: 'a1', xy: [5.6, 9.7], weight: 14},
    {idx: 4, name: 'テイルフェザー', areaId: 'a2', xy: [33.1, 23.3], weight: 3},
    {idx: 5, name: '不浄の三塔', areaId: 'a2', xy: [16.5, 23.5], weight: 3},
    {idx: 6, name: 'モグモグホーム', areaId: 'a3', xy: [27.7, 34.2], weight: 8},
    {idx: 7, name: '白亜の宮殿', areaId: 'a3', xy: [10.7, 29.0], weight: 3}
  ]
}
