import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function _uuid () {
  let uuid = ''
  let i
  let random
  for (i = 0; i < 32; i++) {
    random = Math.random() * 16 | 0

    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuid += '-'
    }
    uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16)
  }
  return uuid
}

const state = {
  tresuares: [
    // { uuid: 'zZdfA', areaId: 'a0', xy: [23, 23], owner: 'Owner Name', complete: false }
  ],
  added: null,
  updated: null,
  deleted: null,
  currentLocation: {
    areaId: 'a0',
    latlng: [126, 126],
    zoom: 1
  },
  routes: [
  ]
}

const actions = {
  changeArea ({ commit }, areaId) {
    // console.log('actions: changeArea ' + areaId)
    commit('CHANGE_AREA', {
      areaId
    })
  },
  changeLatlng ({ commit }, latlng) {
    // console.log('actions: changeLatlng ' + latlng)
    commit('CHANGE_LATLNG', {
      latlng
    })
  },
  changeZoom ({ commit }, zoom) {
    // console.log('actions: changeZoom ' + zoom)
    commit('CHANGE_ZOOM', {
      zoom
    })
  },
  addTreasure ({ commit }, tresuares) {
    // console.log('actions: addTreasure ' + treasure)
    tresuares.forEach(t => {
      t.uuid = _uuid()
    })
    commit('ADD_TRESUARE', {
      tresuares
    })
  },
  updateTresuare ({ commit }, tresuares) {
    // console.log('actions: updateTresuare ')
    commit('UPD_TRESUARE', {
      tresuares
    })
  },
  deleteTresuare ({ commit }, tresuares) {
    // console.log('actions: deleteTresuare ')
    commit('DEL_TRESUARE', {
      tresuares
    })
  },
  moveTresuare ({ commit }, location) {
    // console.log('actions: moveTresuare ')
    commit('MOVE_TRESUARE', {
      location
    })
  },
  refreshTresuare ({ commit }, tresuares) {
    tresuares.forEach(t => {
      t.uuid = _uuid()
    })
    commit('REFLESH_TRESUARE', {
      tresuares
    })
  },
  refreshRoute ({ commit }, routes) {
    routes.forEach(r => {
      r.uuid = _uuid()
    })
    commit('REFLESH_ROUTE', {
      routes
    })
  }}

const mutations = {
  CHANGE_AREA (state, { areaId }) {
    // console.log('mutations: CHANGE_AREA : areaId = ' + areaId)
    state.currentLocation.areaId = areaId
  },
  CHANGE_LATLNG (state, { latlng }) {
    // console.log('mutations: CHANGE_LATLNG : latlng = ' + latlng)
    state.currentLocation.latlng = latlng
  },
  CHANGE_ZOOM (state, { zoom }) {
    // console.log('mutations: CHANGE_ZOOM : zoom = ' + zoom)
    state.currentLocation.zoom = zoom
  },
  ADD_TRESUARE (state, { tresuares }) {
    // console.log('mutations: ADD_TRESUARE : uuid = ' + tresuares)
    state.tresuares = state.tresuares.concat(tresuares)
    state.added = tresuares
  },
  MOVE_TRESUARE (state, { location }) {
    // console.log('mutations: MOVE_TRESUARE : uuid = ' + location.uuid + ' xy = ' + location.xy)
    let tresuare = state.tresuares.find(e => e.uuid === location.uuid)
    tresuare.xy = location.xy
  },
  UPD_TRESUARE (state, { tresuares }) {
    tresuares.forEach(t => {
      // console.log('mutations: UPD_TRESUARE : uuid = ' + t.uuid)
    })
    state.updated = tresuares
  },
  DEL_TRESUARE (state, { tresuares }) {
    tresuares.forEach(t => {
      let index = state.tresuares.findIndex(e => e.uuid === t.uuid)
      // console.log('mutations: DEL_TRESUARE : uuid = ' + t.uuid + ' index = ' + index)
      state.tresuares.splice(index, 1)
    })
    state.deleted = tresuares
  },
  REFLESH_TRESUARE (state, { tresuares }) {
    // console.log('mutations: REFLESH_TRESUARE : length = ' + tresuares.length)
    state.deleted = state.tresuares
    state.tresuares = [].concat(tresuares)
    state.added = tresuares
  },
  REFLESH_ROUTE (state, { routes }) {
    // console.log('mutations: REFLESH_ROUTE : length = ' + routes.length)
    state.routes = [].concat(routes)
  }
}

const getters = {
  currentAreaId: state => {
    // console.log('getters: currentAreaId ' + state.currentLocation.areaId)
    return state.currentLocation.areaId
  },
  currentLatlng: state => {
    // console.log('getters: currentLatlng ' + state.currentLocation.latlng)
    return state.currentLocation.latlng
  },
  currentZoom: state => {
    // console.log('getters: currentZoom ' + state.currentLocation.zoom)
    return state.currentLocation.zoom
  },
  tresuares: state => {
    // console.log('getters: tresuares ' + state.tresuares)
    return state.tresuares
  },
  addedTresuare: state => {
    // console.log('getters: addedTresuare ' + state.added)
    return state.added
  },
  deletedTresuare: state => {
    // console.log('getters: deletedTresuare ' + state.deleted)
    return state.deleted
  },
  updatedTresuare: state => {
    // console.log('getters: updatedTresuare ' + state.updated)
    return state.updated
  },
  routes: state => {
    // console.log('getters: route ' + state.routes.length)
    return state.routes
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})
