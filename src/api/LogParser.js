import config from '@/config'

const ptn1 = /\([^A-Z]?[^A-Z]?([A-Z][a-z'".]+\s[A-Z][a-z'".]+)\)\s.*?(.*?) \(\s([0-9.]+)\s\s,\s([0-9.]+)\s\).*?$/
const ptn2 = /\(([A-Z][a-z'".]+\s[A-Z][a-z'".]+)\)\s(.*?)(?:[@＠])(.*?)$/
const ptn3 = /([0-9][0-9.]*)\s*,\s*([0-9][0-9.]*)/

function getAreaId (name) {
  let aliase = config.aliases.find(a => a.aliase === name)
  if (aliase === undefined) return null
  return aliase.id
}

function getXy (areaId, posName) {
  let area = config.areas.find(a => a.id === areaId)
  if (area === undefined) return null
  let location = area.locations.find(l => l.name === posName)
  if (location === undefined) return null
  return location.xy
}

// 地図クリックで表示される標準の座標指定
function parsePtn1 (owner, areaName, x, y) {
  let areaId = getAreaId(areaName)
  if (areaId) {
    return {
      owner: owner,
      areaId: areaId,
      xy: [Number(x), Number(y)],
      complete: false
    }
  }
  return null
}

// カスタマイズ座標指定
function parsePtn2 (owner, pos, areaName) {
  let areaId = getAreaId(areaName)
  let match
  if (areaId) {
    let posText = toHalf(pos).toUpperCase()
    let xy = getXy(areaId, posText)
    if (xy !== null) {
      return {owner, areaId, xy}
    } else if ((match = ptn3.exec(posText)) !== null) {
      xy = [Number(match[1]), Number(match[2])]
      return {owner, areaId, xy}
    } else {
      xy = [2, 40]
      return {owner, areaId, xy, remakr: pos}
    }
  }
  return null
}

function toHalf (val) {
  let halfVal = val.replace(/[！-～]/g,
    c => {
      return String.fromCharCode(c.charCodeAt(0) - 0xFEE0)
    }
  )
  return halfVal.replace(/”/g, '"')
    .replace(/’/g, '\'')
    .replace(/‘/g, '`')
    .replace(/￥/g, '\\')
    .replace(/\u{3000}/g, ' ')
    .replace(/〜/g, '~')
}

export default {
  parse (text) {
    let tresuares = []
    let lines = text.split(/\r\n|\r|\n/)
    for (let i = 0; i < lines.length; i++) {
      let line = lines[i]
      let tresuare = null
      let m
      // 通常パターン
      if ((m = ptn1.exec(line)) !== null) {
        tresuare = parsePtn1(m[1], m[2], m[3], m[4])
      } else if ((m = ptn2.exec(line)) !== null) {
        tresuare = parsePtn2(m[1], m[2], m[3])
      }
      if (tresuare !== null) {
        tresuares.push(tresuare)
      }
    }
    return tresuares
  }
}
