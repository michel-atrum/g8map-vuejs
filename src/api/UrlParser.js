import LZString from 'lz-string'
// import URI from 'urijs'

function compress (tresuares) {
  let list = []
  tresuares.forEach(t => {
    list.push({
      o: t.owner,
      a: t.areaId,
      x: t.xy[0],
      y: t.xy[1],
      r: t.remark
    })
  })
  let serial = JSON.stringify({v: 0.1, l: list})
  let query = LZString.compressToEncodedURIComponent(serial)
  return query
}

function decompress (param) {
  let tresuares = []
  if (param) {
    let serial = LZString.decompressFromEncodedURIComponent(param)
    let json = JSON.parse(serial)
    if (Array.isArray(json.l)) {
      json.l.forEach(e => {
        tresuares.push({
          owner: e.o,
          areaId: e.a,
          xy: [e.x, e.y],
          remark: e.r
        })
      })
    }
  }
  return tresuares
}

export default {
  setQuery (location, tresuares) {
    // let url = new URI(location.toString())
    // url.hash('#/?_c=' + compress(tresuares)) // vue-router hash mode
    let url = location.protocol + '//' + location.host +
     location.pathname + '#/?_c=' + compress(tresuares)
    return url
  },
  parseQuery (query) {
    return decompress(query['_c'])
  }
}
